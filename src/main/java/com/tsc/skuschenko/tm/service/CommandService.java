package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.ICommandService;
import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(final AbstractCommand abstractCommand) {
        if (abstractCommand == null) return;
        commandRepository.add(abstractCommand);
    }

    @Override
    public Collection<AbstractCommand> getArgs() {
        return commandRepository.getArguments();
    }

    @Override
    public AbstractCommand getCommandByArg(final String name) {
        return commandRepository.getCommandByArg(name);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commandRepository.getCommandByName(name);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public Collection<String> getListCommandNames() {
        return commandRepository.getCommandNames();
    }

}
