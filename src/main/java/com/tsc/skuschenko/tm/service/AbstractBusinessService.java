package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IBusinessRepository;
import com.tsc.skuschenko.tm.api.service.IBusinessService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    IBusinessRepository<E> entityRepository;

    public AbstractBusinessService(IBusinessRepository<E> entityRepository) {
        super(entityRepository);
        this.entityRepository = entityRepository;
    }

    @Override
    public E changeStatusById(
            final String userId, final String id, final Status status
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        final E entity = findOneById(userId, id);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeStatusByIndex(
            final String userId, final Integer index, final Status status
    ) {
        if (index == null || index < 0) {
            throw new IndexIncorrectException(index);
        }
        if (status == null) throw new EmptyStatusException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeStatusByName(
            final String userId, final String name, final Status status
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        final E entity = findOneByName(userId, name);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setStatus(status);
        return entity;
    }

    @Override
    public void clear(String userId) {
        entityRepository.clear(userId);
    }

    @Override
    public E completeById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findOneById(userId, id);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E completeByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) {
            throw new IndexIncorrectException(index);
        }
        final E entity = findOneByIndex(userId, index);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E completeByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(userId, name);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (comparator == null) return null;
        return entityRepository.findAll(userId, comparator);
    }

    @Override
    public List<E> findAll(String userId) {
        return entityRepository.findAll(userId);
    }

    @Override
    public E findOneById(final String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return entityRepository.findOneById(userId, id);
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) {
            throw new IndexIncorrectException(index);
        }
        return entityRepository.findOneByIndex(userId, index);
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return entityRepository.findOneByName(userId, name);
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return entityRepository.removeOneById(userId, id);
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) {
            throw new IndexIncorrectException(index);
        }
        return entityRepository.removeOneByIndex(userId, index);
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return entityRepository.removeOneByName(userId, name);
    }

    @Override
    public E startById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findOneById(userId, id);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) {
            throw new IndexIncorrectException(index);
        }
        final E entity = findOneByIndex(userId, index);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(userId, name);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E updateOneById(
            final String userId, final String id, final String name,
            final String description
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneById(userId, id);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateOneByIndex(
            final String userId, final Integer index, final String name,
            final String description
    ) {
        if (index == null || index < 0) {
            throw new IndexIncorrectException(index);
        }
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}
