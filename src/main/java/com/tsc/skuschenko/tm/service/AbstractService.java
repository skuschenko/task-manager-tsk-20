package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.model.AbstractEntity;

import java.util.List;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> entityRepository;

    public AbstractService(final IRepository<E> entityRepository) {
        this.entityRepository = entityRepository;
    }

    @Override
    public void add(final E entity) {
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entityRepository.add(entity);
    }

    @Override
    public void clear() {
        entityRepository.clear();
    }

    @Override
    public List<E> findAll() {
        return entityRepository.findAll();
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return entityRepository.findById(id);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) {
            final String simpleName = entity.getClass().getSimpleName();
            throw new EntityNotFoundException(simpleName);
        }
        entityRepository.remove(entity);
    }

    @Override
    public E removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return entityRepository.removeById(id);
    }

}
