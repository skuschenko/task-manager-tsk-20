package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.ILogService;

import java.io.IOException;
import java.util.logging.*;

public class LogService implements ILogService {

    private final Logger commands = Logger.getLogger("COMMANDS");

    private final Logger errors = Logger.getLogger("ERRORS");

    private final LogManager manager = LogManager.getLogManager();

    private final Logger messages = Logger.getLogger("MESSAGES");

    private final Logger root = Logger.getLogger("");

    {
        init();
        registry(commands, "./commands.txt", false);
        registry(messages, "./messages.txt", true);
        registry(errors, "./errors.txt", true);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    private void init() {
        try {
            manager.readConfiguration(
                    LogService.class.getResourceAsStream("/logger.properties")
            );
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    public void registry(
            final Logger logger, final String filename, final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

}
