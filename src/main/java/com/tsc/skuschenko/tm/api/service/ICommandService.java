package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand abstractCommand);

    Collection<AbstractCommand> getArgs();

    AbstractCommand getCommandByArg(String name);

    AbstractCommand getCommandByName(String name);

    Collection<AbstractCommand> getCommands();

    Collection<String> getListArgumentName();

    Collection<String> getListCommandNames();

}
