package com.tsc.skuschenko.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
