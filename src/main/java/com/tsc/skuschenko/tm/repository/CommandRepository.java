package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments =
            new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands =
            new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand abstractCommand) {
        final String arg = abstractCommand.arg();
        final String name = abstractCommand.name();
        if (arg != null && !arg.isEmpty()) arguments.put(arg, abstractCommand);
        if (name != null && !name.isEmpty())
            commands.put(name, abstractCommand);
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        final List<AbstractCommand> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            result.add(command);
        }
        return result;
    }

    @Override
    public Collection<String> getCommandArgs() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.arg();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public AbstractCommand getCommandByArg(final String name) {
        return arguments.get(name);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commands.get(name);
    }

    @Override
    public Collection<String> getCommandNames() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        final List<AbstractCommand> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            result.add(command);
        }
        return result;
    }

}
