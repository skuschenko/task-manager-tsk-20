package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.model.Task;

public class TaskRepository extends AbstractBusinessRepository<Task>
        implements ITaskRepository {
}
