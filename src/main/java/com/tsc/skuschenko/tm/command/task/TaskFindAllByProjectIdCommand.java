package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "find all task by project id";

    private final String NAME = "find-all-task-by-project-id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("project id");
        final String value = TerminalUtil.nextLine();
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        final List<Task> tasks
                = projectTaskService.findAllTaskByProjectId(userId, value);
        if (tasks == null || tasks.size() == 0) {
            throw new TaskNotFoundException();
        }
        tasks.forEach(item -> showTask(item));
    }

    @Override
    public String name() {
        return NAME;
    }

}
