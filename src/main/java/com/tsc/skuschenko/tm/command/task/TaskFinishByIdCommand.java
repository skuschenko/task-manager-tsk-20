package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "finish task by id";

    private final String NAME = "task-finish-by-id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("id");
        final String value = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        final Task task = taskService.completeById(userId, value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public String name() {
        return NAME;
    }

}
