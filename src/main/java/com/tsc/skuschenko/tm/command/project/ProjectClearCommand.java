package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;

public class ProjectClearCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "clear all projects";

    private final String NAME = "project-clear";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        projectTaskService.clearProjects(userId);
    }

    @Override
    public String name() {
        return NAME;
    }

}
