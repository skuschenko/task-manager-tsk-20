package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "change project by name";

    private final String NAME = "project-change-status-by-name";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("name");
        final String value = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        Project project = projectService.findOneByName(userId, value);
        if (project == null) throw new ProjectNotFoundException();
        project = projectService.changeStatusByName(
                userId, value, readProjectStatus()
        );
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public String name() {
        return NAME;
    }

}
