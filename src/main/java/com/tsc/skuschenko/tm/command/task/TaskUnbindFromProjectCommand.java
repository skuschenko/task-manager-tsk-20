package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "unbind task from project";

    private final String NAME = "unbind-task-from-project";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("project id");
        final String projectId = TerminalUtil.nextLine();
        showParameterInfo("task id");
        final String taskId = TerminalUtil.nextLine();
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        Task task = projectTaskService.unbindTaskFromProject(
                userId, projectId, taskId
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public String name() {
        return NAME;
    }

}
