package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectFinishByIdCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "finish project by id";

    private final String NAME = "project-finish-by-id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("id");
        final String value = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        final Project project = projectService.completeById(userId, value);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public String name() {
        return NAME;
    }

}
