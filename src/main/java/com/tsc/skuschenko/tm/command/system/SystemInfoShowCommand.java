package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.util.NumberUtil;

public class SystemInfoShowCommand extends AbstractCommand {

    private final String ARGUMENT = "-i";

    private final String DESCRIPTION = "info";

    private final String NAME = "info";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final long availableProcessors
                = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemotyVal = isMaxMemory
                ? "no limit"
                : NumberUtil.formatSize(maxMemory);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        final String totalMemoryResult = "Total memory available to JVM: "
                + NumberUtil.formatSize(totalMemory);
        final String availableProcResult = "Available processors (cores): "
                + availableProcessors;
        final String freeMemoryResult = "Free memory: "
                + NumberUtil.formatSize(freeMemory);
        final String maxMemotyValResult = "Maximum memory: " + maxMemotyVal;
        final String usedMemoryResult = "Used memory by JVM: "
                + NumberUtil.formatSize(usedMemory);
        showOperationInfo(NAME);
        System.out.println(availableProcResult);
        System.out.println(freeMemoryResult);
        System.out.println(maxMemotyValResult);
        System.out.println(totalMemoryResult);
        System.out.println(usedMemoryResult);
    }

    @Override
    public String name() {
        return NAME;
    }

}
