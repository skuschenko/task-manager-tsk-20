package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;

public class AllCommandsShowCommand extends AbstractCommand {

    private final String DESCRIPTION = "commands";

    private final String NAME = "commands";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        final Collection<String> names =
                serviceLocator.getCommandService().getListCommandNames();
        for (final String name : names) {
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String name() {
        return NAME;
    }

}
