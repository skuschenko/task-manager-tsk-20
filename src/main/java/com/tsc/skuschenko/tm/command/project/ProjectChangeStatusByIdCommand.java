package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "change project by id";

    private final String NAME = "project-change-status-by-id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("id");
        final String valueId = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        Project project = projectService.findOneById(userId, valueId);
        if (project == null) throw new ProjectNotFoundException();
        project = projectService.changeStatusById(
                userId, valueId, readProjectStatus()
        );
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public String name() {
        return NAME;
    }

}
