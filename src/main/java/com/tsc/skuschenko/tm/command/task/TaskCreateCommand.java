package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "create new task";

    private final String NAME = "task-create";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("name");
        final String name = TerminalUtil.nextLine();
        showParameterInfo("description");
        final String description = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        final Task task = taskService.add(name, description);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public String name() {
        return NAME;
    }

}
