package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "remove project by index";

    private final String NAME = "project-remove-by-index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("index");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final IProjectService projectService
                = serviceLocator.getProjectService();
        final Project project = projectService.removeOneByIndex(userId, value);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return NAME;
    }

}
