package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "start task by index";

    private final String NAME = "task-start-by-index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("index");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final ITaskService taskService = serviceLocator.getTaskService();
        final Task task = taskService.startByIndex(userId, value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public String name() {
        return NAME;
    }

}