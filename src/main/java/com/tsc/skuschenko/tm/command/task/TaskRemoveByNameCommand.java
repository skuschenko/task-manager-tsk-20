package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "remove task by name";

    private final String NAME = "task-remove-by-name";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("name");
        final String value = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        final Task task = taskService.removeOneByName(userId, value);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public String name() {
        return NAME;
    }

}
