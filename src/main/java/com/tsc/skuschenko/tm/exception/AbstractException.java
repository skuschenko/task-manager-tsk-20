package com.tsc.skuschenko.tm.exception;

public class AbstractException extends RuntimeException {

    public AbstractException(final String value) {
        super(value);
    }

}
