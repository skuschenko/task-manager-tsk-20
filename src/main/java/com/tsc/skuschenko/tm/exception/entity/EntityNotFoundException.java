package com.tsc.skuschenko.tm.exception.entity;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException(final String entity) {
        super("Error! " + entity + " not found...");
    }

}
