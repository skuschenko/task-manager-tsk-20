package com.tsc.skuschenko.tm.enumerated;

public enum Role {

    ADMIN("Administrator"),

    USER("User");

    private String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
